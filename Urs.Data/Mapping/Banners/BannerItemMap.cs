using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Mapping;
using Urs.Data.Domain.Banners;

namespace Urs.Data.Mapping.Banners
{
    public partial class BannerItemMap : UrsEntityTypeConfiguration<BannerItem>
    {
        public override void Configure(EntityTypeBuilder<BannerItem> builder)
        {
            builder.ToTable(nameof(BannerItem));
            builder.HasKey(x => x.Id);

            builder.HasOne(bs => bs.Banner)
                .WithMany(c => c.Items)
                .HasForeignKey(bs => bs.BannerId).IsRequired();


            base.Configure(builder);
        }
    }
}