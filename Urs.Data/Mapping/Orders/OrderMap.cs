
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Mapping.Orders
{
    public partial class OrderMap : UrsEntityTypeConfiguration<Order>
    {
        public override void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable(nameof(Order));
            builder.HasKey(o => o.Id);
            builder.Property(o => o.OrderShipping).HasColumnType("decimal(18, 4)");
            builder.Property(o => o.PaymentMethodAdditionalFee).HasColumnType("decimal(18, 4)");
            builder.Property(o => o.OrderTotal).HasColumnType("decimal(18, 4)");
            builder.Property(o => o.RefundedAmount).HasColumnType("decimal(18, 4)");

            builder.Ignore(o => o.OrderStatus);
            builder.Ignore(o => o.PaymentStatus);
            builder.Ignore(o => o.ShippingStatus);
            
            builder.HasOne(o => o.User)
                .WithMany()
                .HasForeignKey(o => o.UserId);
            
            builder.HasOne(o => o.ShippingAddress)
                .WithMany()
                .HasForeignKey(o => o.ShippingAddressId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            base.Configure(builder);
        }
    }
}