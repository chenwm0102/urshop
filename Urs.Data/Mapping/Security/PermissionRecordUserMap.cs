
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Security;

namespace Urs.Data.Mapping.Security
{
    public partial class PermissionRecordUserMap : UrsEntityTypeConfiguration<PermissionRecordUserMapping>
    {
        public override void Configure(EntityTypeBuilder<PermissionRecordUserMapping> builder)
        {

            builder.ToTable(nameof(PermissionRecordUserMapping));
            builder.HasKey(mapping => new { mapping.PermissionRecordId, mapping.UserRoleId });

            builder.HasOne(mapping => mapping.UserRole)
                .WithMany()
                .HasForeignKey(mapping => mapping.UserRoleId)
                .IsRequired();

            builder.HasOne(mapping => mapping.PermissionRecord)
                .WithMany(record => record.UserRoles)
                .HasForeignKey(mapping => mapping.PermissionRecordId)
                .IsRequired();

            builder.Ignore(mapping => mapping.Id);

            base.Configure(builder);
        }
    }
}