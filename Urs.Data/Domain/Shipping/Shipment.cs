using Urs.Core;
using System;
using System.Collections.Generic;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Domain.Shipping
{
    /// <summary>
    /// Represents a shipment
    /// </summary>
    public partial class Shipment : BaseEntity
    {
        private ICollection<ShipmentOrderItem> _shipmentorderItems;
        /// <summary>
        /// 订单Id
        /// </summary>
        public virtual int OrderId { get; set; }
        /// <summary>
        /// 配送方式
        /// </summary>
        public string ShippingMethod { get; set; }
        /// <summary>
        /// 快递公司
        /// </summary>
        public string ExpressName { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public virtual string TrackingNumber { get; set; }

        public virtual decimal? TotalWeight { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        public virtual DateTime? ShippedTime { get; set; }
        /// <summary>
        /// 送达时间
        /// </summary>
        public virtual DateTime? DeliveryTime { get; set; }
        public virtual DateTime CreateTime { get; set; }
        public virtual Order Order { get; set; }
        public virtual ICollection<ShipmentOrderItem> ShipmentorderItems
        {
            get { return _shipmentorderItems ?? (_shipmentorderItems = new List<ShipmentOrderItem>()); }
            protected set { _shipmentorderItems = value; }
        }
    }
}