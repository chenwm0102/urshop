namespace Urs.Data.Domain.Orders
{
    /// <summary>
    /// 订单状态
    /// </summary>
    public enum OrderStatus : int
    {
        /// <summary>
        /// 待付款
        /// </summary>
        Pending = 10,
        /// <summary>
        /// 待发货
        /// </summary>
        Processing = 20,
        /// <summary>
        /// 待收货
        /// </summary>
        Delivering=23,
        /// <summary>
        /// 待评论
        /// </summary>
        Reviewing = 25,
        /// <summary>
        /// 完成
        /// </summary>
        Complete = 30,
        /// <summary>
        /// 取消
        /// </summary>
        Cancelled = 40
    }
}
