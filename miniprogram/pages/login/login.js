var app = getApp()
import api from '../../api/api'
import {
  weixinopenlogin
} from '../../api/conf'
import {
  accountlogin
} from '../../api/conf'

Page({

  data: {

  },
  onLoad: function(options) {

  },
  onReady: function() {

  },
  onShow: function() {

  },
  formSubmit: function(e) {
    var that = this
    if (e.detail.value.phone == '') {
      wx.showToast({
        title: '手机号不能为空',
        icon: 'none',
      })
      return
    } else if (e.detail.value.pwd == '') {
      wx.showToast({
        title: '密码不能为空',
        icon: 'none',
      })
      return
    }
    wx.showLoading({
      title: '正在登录',
      mask: true
    })
    api.post(accountlogin, {
      Name: e.detail.value.phone,
      Password: e.detail.value.pwd
    }).then((datas) => {
      var sdata = datas.Data
      wx.login({
        success(res) {
          if (res.code) {
            wx.request({
              url: weixinopenlogin,
              data: {
                code: res.code,
                guid: sdata.Guid
              },
              method: 'get',
              header: {
                "context-type": 'application/json',
              },
              success: function(rep) {
                wx.setStorageSync('guid', rep.data.Data.guid)
                wx.setStorageSync('token', rep.data.Data.token)
                wx.setStorageSync('openId', rep.data.Data.openId)
                wx.hideLoading()
                wx.switchTab({
                  url: '/pages/index/index'
                })
              }
            })
          } else {

          }
        },
        fail(err) {
          wx.showToast({
            title: '登录超时',
            icon: 'none'
          })
        }
      })
    }).catch((res) => {
      wx.showToast({
        title: '密码错误',
        icon: 'none',
      })
      wx.hideLoading()
    })
  },
  gohome: function() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  }
})