﻿using System;
using FluentValidation;
using FluentValidation.Attributes;
using Urs.Core.Infrastructure;

namespace Urs.Framework.Validators
{
    public class UrsValidatorFactory : AttributedValidatorFactory
    {
        public override IValidator GetValidator(Type type)
        {
            if (type != null)
            {
                var attribute = (ValidatorAttribute)Attribute.GetCustomAttribute(type, typeof(ValidatorAttribute));
                if ((attribute != null) && (attribute.ValidatorType != null))
                {
                    var instance = EngineContext.Current.ResolveUnregistered(attribute.ValidatorType);
                    return instance as IValidator;
                }
            }
            return null;

        }
    }

    public abstract class BaseUrsValidator<T> : AbstractValidator<T> where T : class
    {
    }
}