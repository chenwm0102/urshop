﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Tasks;
using Urs.Framework;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Tasks
{
    [Validator(typeof(ScheduleTaskValidator))]
    public partial class ScheduleTaskModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.System.ScheduleTasks.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.System.ScheduleTasks.Seconds")]
        public int Seconds { get; set; }

        [UrsDisplayName("Admin.System.ScheduleTasks.Enabled")]
        public bool Enabled { get; set; }

        [UrsDisplayName("Admin.System.ScheduleTasks.StopOnError")]
        public bool StopOnError { get; set; }

        [UrsDisplayName("Admin.System.ScheduleTasks.LastStart")]
        public string LastStartUtc { get; set; }

        [UrsDisplayName("Admin.System.ScheduleTasks.LastEnd")]
        public string LastEndUtc { get; set; }

        [UrsDisplayName("Admin.System.ScheduleTasks.LastSuccess")]
        public string LastSuccessUtc { get; set; }
    }
}