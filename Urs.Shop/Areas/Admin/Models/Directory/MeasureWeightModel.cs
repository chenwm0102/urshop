﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Directory;
using Urs.Framework;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Directory
{
    [Validator(typeof(MeasureWeightValidator))]
    public partial class MeasureWeightModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Configuration.Measures.Weights.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Configuration.Measures.Weights.Fields.SystemKeyword")]
        
        public string SystemKeyword { get; set; }

        [UrsDisplayName("Admin.Configuration.Measures.Weights.Fields.Ratio")]
        [UIHint("Decimal8")]
        public decimal Ratio { get; set; }

        [UrsDisplayName("Admin.Configuration.Measures.Weights.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [UrsDisplayName("Admin.Configuration.Measures.Weights.Fields.IsPrimaryWeight")]
        public bool IsPrimaryWeight { get; set; }
    }
}