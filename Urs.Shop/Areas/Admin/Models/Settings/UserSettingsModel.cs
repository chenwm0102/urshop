﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Urs.Admin.Models.Users;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class UserSettingsModel : BaseModel, ISettingsModel
    {
        public UserSettingsModel()
        {
            UserSettings = new UserInfoSettingsModel();
            AddressSettings = new AddressSettingsModel();
            ExternalAuthenticationSettings = new ExternalAuthenticationSettingsModel();
            BillingAddressSettings = new BillingAddressSettingsModel();
        }
        public UserInfoSettingsModel UserSettings { get; set; }
        public AddressSettingsModel AddressSettings { get; set; }
        public ExternalAuthenticationSettingsModel ExternalAuthenticationSettings { get; set; }
        public BillingAddressSettingsModel BillingAddressSettings { get; set; }

        #region Nested classes

        public partial class UserInfoSettingsModel : BaseModel, ISettingsModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.User.UsernamesEnabled")]
            public bool UsernamesEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.UserRegistrationType")]
            public int UserRegistrationType { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AllowUsersToUploadAvatars")]
            public bool AllowUsersToUploadAvatars { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.DefaultAvatarEnabled")]
            public bool DefaultAvatarEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.NotifyNewUserRegistration")]
            public bool NotifyNewUserRegistration { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.HideBackInStockSubscriptionsTab")]
            public bool HideBackInStockSubscriptionsTab { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.UserNameFormat")]
            public int UserNameFormat { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.DefaultPasswordFormat")]
            public int DefaultPasswordFormat { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.NewsletterEnabled")]
            public bool NewsletterEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.GenderEnabled")]
            public bool GenderEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.DateOfBirthEnabled")]
            public bool DateOfBirthEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.CompanyEnabled")]
            public bool CompanyEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.CompanyRequired")]
            public bool CompanyRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.StreetAddressEnabled")]
            public bool StreetAddressEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.StreetAddressRequired")]
            public bool StreetAddressRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.StreetAddress2Enabled")]
            public bool StreetAddress2Enabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.StreetAddress2Required")]
            public bool StreetAddress2Required { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.ZipPostalCodeEnabled")]
            public bool ZipPostalCodeEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.ZipPostalCodeRequired")]
            public bool ZipPostalCodeRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.ProvinceEnabled")]
            public bool ProvincesEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.PhoneEnabled")]
            public bool PhoneEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.PhoneRequired")]
            public bool PhoneRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.FaxEnabled")]
            public bool FaxEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.FaxRequired")]
            public bool FaxRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AcceptPrivacyPolicyEnabled")]
            public bool AcceptPrivacyPolicyEnabled { get; set; }
        }

        public partial class AddressSettingsModel : BaseModel, ISettingsModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.CompanyEnabled")]
            public bool CompanyEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.CompanyRequired")]
            public bool CompanyRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.StreetAddressEnabled")]
            public bool StreetAddressEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.StreetAddressRequired")]
            public bool StreetAddressRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.StreetAddress2Enabled")]
            public bool StreetAddress2Enabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.StreetAddress2Required")]
            public bool StreetAddress2Required { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.ZipPostalCodeEnabled")]
            public bool ZipPostalCodeEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.ZipPostalCodeRequired")]
            public bool ZipPostalCodeRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.ProvinceEnabled")]
            public bool ProvincesEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.PhoneEnabled")]
            public bool PhoneEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.PhoneRequired")]
            public bool PhoneRequired { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.FaxEnabled")]
            public bool FaxEnabled { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.User.AddressFormFields.FaxRequired")]
            public bool FaxRequired { get; set; }
        }

        public partial class BillingAddressSettingsModel : BaseModel, ISettingsModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.User.BillingAddressFormFields.Enabled")]
            public bool BillingEnabled { get; set; }
        }

        public partial class ExternalAuthenticationSettingsModel : BaseModel, ISettingsModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.User.ExternalAuthenticationAutoRegisterEnabled")]
            public bool AutoRegisterEnabled { get; set; }
        }
        #endregion
    }
}