﻿using Microsoft.AspNetCore.Mvc;
using Urs.Admin.Models.Users;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Configuration;
using Urs.Services.Authentication;
using Urs.Services.Configuration;
using Urs.Services.Users;
using Urs.Services.Localization;
using Urs.Services.Logging;
using Urs.Framework.Security;

namespace Urs.Admin.Controllers
{
    public partial class LoginController : BaseAdminController
    {
        #region Fields
        private readonly ISettingService _settingService;

        private readonly ILocalizationService _localizationService;
        private readonly IUserRegistrationService _userRegistrationService;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IActivityLogService _activityLogService;
        private readonly UserSettings _userSettings;

        #endregion

        #region Ctor

        public LoginController(ISettingService settingService,
             ILocalizationService localizationService,
            IUserRegistrationService userRegistrationService,
            IUserService userService,
            IAuthenticationService authenticationService,
            IActivityLogService activityLogService,
            UserSettings userSettings)
        {
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._userRegistrationService = userRegistrationService;
            this._userService = userService;
            this._authenticationService = authenticationService;
            this._activityLogService = activityLogService;
            this._userSettings = userSettings;
        }

        #endregion

        #region Methods
        [HttpsRequirement(SslRequirement.Yes)]
        public IActionResult Index(bool? checkoutAsGuest)
        {
            var model = new AdminLoginModel();
            model.CheckoutAsGuest = checkoutAsGuest.HasValue ? checkoutAsGuest.Value : false;
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(AdminLoginModel model, string returnUrl, bool captchaValid)
        {
            if (ModelState.IsValid)
            {
                if (_userRegistrationService.Validate(model.AccountName, model.Password))
                {
                    var user = _userService.GetUsersByUsernameOrPhoneOrEmail(model.AccountName);

                    //sign in new user
                    _authenticationService.SignIn(user, model.RememberMe);

                    //activity log
                    _activityLogService.InsertActivity("PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), user);

                    return Json(new { success = 1,msg= "登入成功,正在跳转...", url = "/admin" });
                }
                else
                {
                    return Json(new { error = 1, msg = "密码有误" });
                }
            }

            return Json(new { error = 1, msg = "请检查登陆信息" });
        }

        public IActionResult Logout()
        {
            //standard logout 
            _authenticationService.SignOut();
            return RedirectToAction("Index");
        }
        #endregion
    }
}
