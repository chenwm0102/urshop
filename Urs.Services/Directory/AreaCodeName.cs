﻿namespace Urs.Services.Directory
{
    public class AreaCodeName
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
