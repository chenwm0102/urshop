

using Urs.Data.Domain.Users;

namespace Urs.Services.Authentication.External
{
    public partial interface IExternalAuthorizer
    {
        int AvatarPicture(string imageUrl);
        AuthorizationResult Authorize(OpenAuthParameters parameters, User user);
        AuthorizationResult ApiAuthorize(OpenAuthParameters parameters, User userLoggedIn);
    }
}