using Autofac;
using Urs.Core.Configuration;
using Urs.Core.Infrastructure;
using Urs.Core.Infrastructure.DependencyManagement;
using Urs.Plugin.ExternalAuth.WeixinOpen.Core;

namespace Urs.Plugin.ExternalAuth.WeixinOpen.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, UrsConfig config)
        {
            builder.RegisterType<WeixinOpenProviderAuthorizer>().As<IWeixinOpenProviderAuthorizer>().InstancePerDependency();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}