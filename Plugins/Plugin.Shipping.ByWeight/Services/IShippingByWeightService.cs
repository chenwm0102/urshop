using System.Collections.Generic;
using Urs.Core;
using Urs.Plugin.Shipping.ByWeight.Domain;

namespace Urs.Plugin.Shipping.ByWeight.Services
{
    public partial interface IShippingByWeightService
    { 
        #region Shipping

        void DeleteShippingByWeightRecord(ShippingByWeightRecord shippingByWeightRecord);

        IPagedList<ShippingByWeightRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);

        ShippingByWeightRecord FindRecord(int shippingMethodId, string provinceCode, string cityCode, decimal weight);

        ShippingByWeightRecord GetById(int shippingByWeightRecordId);

        void InsertShippingByWeightRecord(ShippingByWeightRecord shippingByWeightRecord);

        void UpdateShippingByWeightRecord(ShippingByWeightRecord shippingByWeightRecord);

        #endregion

        #region ShippingScopes

        void InsertShippingScope(ShippingScopeByWeightRecord entity);

        IList<ShippingScopeByWeightRecord> GetScopesByPieceRecordId(int shippingByWeightRecordId);

        void DeleteShippingScope(ShippingScopeByWeightRecord entity);

        #endregion
    }
}
