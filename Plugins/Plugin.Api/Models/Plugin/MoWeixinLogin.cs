﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Plugin
{
    /// <summary>
    /// 登陆对象
    /// </summary>
    public class MoWeixinLogin
    {
        public string token { get; set; }
        public string guid { get; set; }
        public string openId { get; set; }
    }
}