﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Api.Models.Media;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models.GoodsReview
{
    /// <summary>
    /// 评论
    /// </summary>
    public partial class MoGoodsReview
    {
        public MoGoodsReview()
        {
            Pictures = new List<MoPicture>();
            var list = new List<MoPicture>();
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/1.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/1-300.jpg" });
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/2.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/2-300.jpg" });
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/3.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/3-300.jpg" });
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/4.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/4-300.jpg" });
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/5.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/5-300.jpg" });
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/6.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/6-300.jpg" });
            list.Add(new MoPicture() { NormalUrl = "http://demo.urselect.com/images/rwimg/7.jpg", BigUrl = "http://demo.urselect.com/images/rwimg/7-300.jpg" });
            var tt = new Random().Next(7);
            Pictures = list.Take(tt).ToList();
        }
        /// <summary>
        /// 评论Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户头像url
        /// </summary>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 评论标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 评论内容
        /// </summary>
        public string ReviewText { get; set; }
        /// <summary>
        /// 评分
        /// </summary>
        public int Rating { get; set; }
        /// <summary>
        /// 商品图片
        /// </summary>
        public IList<MoPicture> Pictures { get; set; }
        /// <summary>
        /// 评论时间
        /// </summary>
        public string CreateTime { get; set; }
    }
}